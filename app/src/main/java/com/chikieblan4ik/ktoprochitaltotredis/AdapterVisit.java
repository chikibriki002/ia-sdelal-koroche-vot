package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterVisit extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Visit> visits;

    AdapterVisit(Context context, ArrayList<Visit> visits) {
        this.ctx = context;
        this.visits  = visits;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return visits.size();
    }

    @Override
    public Object getItem(int position) {
        return visits.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_visitg, parent, false);
        }

        Visit oneVisit = visits.get(position);
        ((TextView) (view).findViewById(R.id.tvDateVisit)).setText(oneVisit.getDateVisit());
        ((TextView) (view).findViewById(R.id.tvTimeVisit)).setText(oneVisit.getTimeVisit());

        return view;
    }
}
