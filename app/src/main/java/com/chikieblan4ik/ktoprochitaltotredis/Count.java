package com.chikieblan4ik.ktoprochitaltotredis;

import android.os.Parcel;
import android.os.Parcelable;

public class Count implements Parcelable {

    private String numberCount;
    private double sumOnCount;

    public Count(String numberCount, double sumOnCount) {
        this.numberCount = numberCount;
        this.sumOnCount = sumOnCount;
    }

    protected Count(Parcel in) {
        numberCount = in.readString();
        sumOnCount = in.readDouble();
    }

    public static final Creator<Count> CREATOR = new Creator<Count>() {
        @Override
        public Count createFromParcel(Parcel in) {
            return new Count(in);
        }

        @Override
        public Count[] newArray(int size) {
            return new Count[size];
        }
    };

    public String getNumberCount() {
        return numberCount;
    }

    public double getSumOnCount() {
        return sumOnCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(numberCount);
        dest.writeDouble(sumOnCount);
    }
}
