package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdapterCard extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Card> cards;

    AdapterCard(Context context, ArrayList<Card> cards) {
        this.ctx = context;
        this.cards = cards;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return cards.size();
    }

    @Override
    public Object getItem(int position) {
        return cards.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
//            view = inflater.inflate(R.layout.item_card, parent, false);
        }
//
//        Card oneCard = cards.get(position);
//        ((TextView) (view).findViewById(R.id.tvTypeCard)).setText(oneCard.getTypeCard());
//        ((TextView) (view).findViewById(R.id.tvNumCard)).setText(oneCard.getNumberCard());
//        ((TextView) (view).findViewById(R.id.tvSumOnCard)).setText(oneCard.getSumOnCard() + "");

        return view;
    }
}
