package com.chikieblan4ik.ktoprochitaltotredis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Information extends AppCompatActivity {

    private TextView tvUrl;
    private ImageView btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);
        getSupportActionBar().hide();
        tvUrl = findViewById(R.id.tvUrl);

        tvUrl.setText(Html.fromHtml("<a href=>Cсылка на сайт с фотками пингвинов</a>"));
        tvUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browseIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://pixabay.com/ru/photos/search/пингвины/"));
                startActivity(browseIntent);
            }
        });

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Information.this, MenuProfileActivity.class);
                startActivity(intent);
            }
        });
    }
}