package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterCredit extends RecyclerView.Adapter<RecyclerAdapterCredit.ViewHolder> {

    private ArrayList<Credit> credits;
    private LayoutInflater mInflater;
    private ItemClickListener mClickListener;

    // data is passed into the constructor
    RecyclerAdapterCredit(Context context, ArrayList<Credit> creditsA) {
        this.mInflater = LayoutInflater.from(context);
        this.credits = creditsA;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_credit, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Credit oneCredit = credits.get(position);
        holder.tvTypeCredit.setText(oneCredit.getTypeCredit());
        holder.tvDateCredit.setText(oneCredit.getDateCredit());
        holder.tvSumCredit.setText(oneCredit.getSumCredit() + "");
    }


    // total number of rows
    @Override
    public int getItemCount() {
        return credits.size();
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTypeCredit;
        TextView tvDateCredit;
        TextView tvSumCredit;

        ViewHolder(View itemView) {
            super(itemView);
            tvTypeCredit = itemView.findViewById(R.id.tvTypeCredit);
            tvDateCredit = itemView.findViewById(R.id.tvDateCredit);
            tvSumCredit = itemView.findViewById(R.id.tvSumCredit);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    // convenience method for getting data at click position
    Credit getItem(int id) {
        return credits.get(id);
    }

    // allows clicks events to be caught
    void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }
}
