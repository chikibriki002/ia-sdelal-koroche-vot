package com.chikieblan4ik.ktoprochitaltotredis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    public static final String TAG = "main";
    Button btnReg;
    EditText etLogin;
    EditText etPassword;

    String login;
    String password;
    static String token;

    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        btnReg = findViewById(R.id.btnReg);
        etLogin = findViewById(R.id.etLogin);
        etPassword = findViewById(R.id.etPassword);

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               clickToSignUp();
            }
        });
//        clickArtem();

    }

    public Link getLoginLink(String login, String password) throws Throwable {
        String url = "https://reqres.in/api/login";
        Map<String, String> myKeysValues = new HashMap<>();
        //    "email": "eve.holt@reqres.in"
        //    "password": "cityslicka"
        // тут должен быть код запроса в aпи, чтобы проверить есть ли такой пользователь, а не это:
        myKeysValues.put("email", "eve.holt@reqres.in");
        myKeysValues.put("password", "cityslicka");
        return new Link(myKeysValues, url);
    }

    public void clickToSignUp() {
        try {
            login = etLogin.getText().toString();
            password = etPassword.getText().toString();

            Link link = getLoginLink(login, password);
            String urlTemp = Api.post(link);
            try {
                JSONObject response = new JSONObject(urlTemp);
                if (response.getString("error") != null) {
                    Toast.makeText(this, response.getString("error"), Toast.LENGTH_SHORT).show();
                    return;
                }

                token = response.getString("token");
                Log.i(TAG, "onClick: " + token);
            } catch (Exception e) {
                e.printStackTrace();
                Log.i(TAG, "onClick: " + e);
            }

//        this.user = new User(token);

            Intent intent = new Intent(MainActivity.this, BankActivity.class);
            intent.putExtra("token", token);
            startActivity(intent);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            Toast.makeText(this, "Нету логина или пароля123", Toast.LENGTH_SHORT).show();
        }

    }

    public void clickArtem() {
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BankActivity.class);
                startActivity(intent);
            }
        });
    }
}