package com.chikieblan4ik.ktoprochitaltotredis;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MenuProfileActivity extends AppCompatActivity {

    public static final String TAG = "menu";
    private TextView btnChPass;
    private TextView btnChLog;
    private TextView btnHistory;
    private TextView btnInfo;
    private TextView btnLogout;
    private ImageView btnBack;

    Map<String, String> keysValues = new HashMap<>();

    String url = "https://reqres.in/api/users/2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_profile);
        getSupportActionBar().hide();
        btnChPass = findViewById(R.id.btnChPass);
        btnChLog = findViewById(R.id.btnChLog);
        btnHistory = findViewById(R.id.btnHistory);
        btnInfo = findViewById(R.id.btnInfo);
        btnLogout = findViewById(R.id.btnLogout);

        clickChangePassword();
        clickChangeLogin();

        btnHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProfileActivity.this, HistoryVisits.class);
                startActivity(intent);
            }
        });

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProfileActivity.this, Information.class);
                startActivity(intent);
            }
        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProfileActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuProfileActivity.this, BankActivity.class);
                startActivity(intent);
            }
        });
    }

    public void changePassPut(String password) { // name -> morpheus &&& job -> zion resident
        keysValues.put("name" , "morpheus");
        keysValues.put("job" , password);
        String urlJson = Api.put(new Link(keysValues, url));
        try {
            String newPassword = new JSONObject(urlJson).getString("job");
            if (newPassword.equals(password)) {
                Toast.makeText(this, "Ну ты палучаеца изменил ПАРОЛЬ вот да", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "есть ошибка в изменении пароля не знаю какая ты лох", Toast.LENGTH_SHORT).show();
            }
            Log.i(TAG, "Новый пароооооооооооль: " + newPassword);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "changePassPut: " + e);
        }
    }

    public void changeLogPut(String login) { // name -> morpheus &&& job -> zion resident
        keysValues.put("name" , "morpheus");
        keysValues.put(login , "zion resident");
        String urlJson = Api.put(new Link(keysValues, url));
        try {
            String newLogin = new JSONObject(urlJson).getString("name");
            if (newLogin.equals(login)) {
                Toast.makeText(this, "Ну ты палучаеца изменил ЛОГИН вот да", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "есть ошибка в изменении логина не знаю какая ты лох", Toast.LENGTH_SHORT).show();
            }
            Log.i(TAG, "Новый логин клево: " + newLogin);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "changeLogPut: " + e);
        }
    }

    public void clickChangePassword() {
        String title = "Изменение пароля";
        String message = "Введите новый пароль";

        android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        ad.setTitle(title);
        ad.setMessage(message);
        final EditText editText = new EditText(this);
        ad.setView(editText);
        ad.setCancelable(true);

        ad.setPositiveButton("Изменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changePassPut(editText.getText().toString());
//                Toast.makeText(MenuProfileActivity.this, "Вы успешно изменили пароль", Toast.LENGTH_SHORT).show();
            }
        });

        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = ad.create();
        btnChPass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }

    public void clickChangeLogin() {
        String title = "Изменение логина";
        String message = "Введите новый логин";

        android.app.AlertDialog.Builder ad = new android.app.AlertDialog.Builder(this);
        ad.setTitle(title);
        ad.setMessage(message);
        final EditText editText = new EditText(this);
        ad.setView(editText);
        ad.setCancelable(true);

        ad.setPositiveButton("Изменить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                changeLogPut(editText.getText().toString());
//                Toast.makeText(MenuProfileActivity.this, "Вы успешно изменили логин!", Toast.LENGTH_SHORT).show();
            }
        });

        ad.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog dialog = ad.create();
        btnChLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });
    }


}