package com.chikieblan4ik.ktoprochitaltotredis;

import android.util.Log;
import android.widget.Adapter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class Api {

    private static final String TAG = "api";
    static String rezult;

     private  Api() {}

    public static String responseFromStream(BufferedReader in) {
        try {
            StringBuilder constructor = new StringBuilder();
            String temp;
            while ((temp = in.readLine()) != null) {
                constructor.append(temp);
            }
            return constructor.toString();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createResponse: " + e);
            return null;
        }
    }

    public static String get(final Link link) {
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = link.openConnection();
                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    rezult = responseFromStream(in);
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "runGet: " + e);
                }
            }
        };

        Thread thGet = new Thread(run);
        try {
            thGet.start();
            thGet.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "thGet: " + e);
        }

        return rezult;
    }

//    public static String createLink(Map<String, String> keysValues) {
//        try {
//            StringBuilder constructor = new StringBuilder();
//            Iterator iterator = keysValues.keySet().iterator();
//            while (iterator.hasNext()) {
//                String key = iterator.next().toString();
//                constructor.append(key);
//                constructor.append("=");
//                constructor.append(keysValues.get(key));
//                constructor.append("&");
//            }
//
//            return constructor.substring(0, constructor.length() - 1);
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(MainActivity.TAG, "createLink: " + e);
//            return null;
//        }
//    }

    public static String post(final Link link) {
        rezult = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = link.openConnection();

                    connection.setRequestMethod("POST");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.get().getBytes());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "run: " + e);
                }
            }
        };

        Thread th2 = new Thread(run);
        try {
            th2.start();
            th2.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "Thread POST: " + e);
        }
        return rezult;
    }

    public static String delete(final Link link) {
        rezult = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = link.openConnection();

                    connection.setRequestMethod("DELETE");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.get().getBytes());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "runDelete: " + e);
                }
            }
        };

        Thread thPost = new Thread(run);
        try {
            thPost.join();
            thPost.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "thDelete: " + e);
        }
        return rezult;
    }

    public static String put(final Link link) {
        rezult = null;
        Runnable run = new Runnable() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = link.openConnection();

                    connection.setRequestMethod("PUT");
                    connection.setDoOutput(true);
                    connection.getOutputStream().write(link.get().getBytes());

                    BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    rezult = responseFromStream(in);

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.i(TAG, "runPut: " + e);
                }
            }
        };
        Thread thPut = new Thread(run);
        try {
            thPut.start();
            thPut.join();
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "thPut: " + e);
        }
        return rezult;
    }
}

