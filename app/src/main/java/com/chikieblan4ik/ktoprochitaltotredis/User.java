package com.chikieblan4ik.ktoprochitaltotredis;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class User implements Parcelable {

    private static final String TAG = "user";
    private String name;
    private String middleName;
    private String login;
    private ArrayList<Card> cards = new ArrayList<>();
    private ArrayList<Count> counts = new ArrayList<>();
    private ArrayList<Credit> credits = new ArrayList<>();
    private ArrayList<Visit> visits = new ArrayList<>();
    private String token;

    // ----------------------------------//
    ArrayList<CardAPI> cardsAPIArray;
    ArrayList<CountAPI> countsAPIArray;
    ArrayList<CreditAPI> creditsAPIArray;

    String urlHistory = "https://reqres.in/api/users/2";
    //----------------------------------//

    public User(String token) {
        this.token = token;
        cardsAPIArray = new ArrayList<>();
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", 4509.95));
        cardsAPIArray.add(new CardAPI("Кредитная карта", "123456654321", 450000.00));
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", 0.12));
        cardsAPIArray.add(new CardAPI("Дебетовая карта", "123456654321", 10000.00));

        countsAPIArray = new ArrayList<>();
        countsAPIArray.add(new CountAPI("12341324132456", 12345.56));
        countsAPIArray.add(new CountAPI("12341324132456", 150.00));
        countsAPIArray.add(new CountAPI("12341324132456", 10000.00));
        countsAPIArray.add(new CountAPI("12341324132456", 400.00));

        creditsAPIArray = new ArrayList<>();
        creditsAPIArray.add(new CreditAPI("Кредит наличными", "26.01.18", 10000.00));
        creditsAPIArray.add(new CreditAPI("Ипотека", "14.12.20", 10.00));
    }

    protected User(Parcel in) {
        name = in.readString();
        middleName = in.readString();
        login = in.readString();
        token = in.readString();
        cards = in.readArrayList(null);
        counts = in.readArrayList(null);
        credits = in.readArrayList(null);
        visits = in.readArrayList(null);
    }

    public boolean addCardToArray(Card card) {
        try {
            this.cards.add(card);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "addCardToArray: " + e);
            return false;
        }
    }

    public Card getOneCard(CardAPI cardAPI) {
        try {
            String typeCard = cardAPI.getTypeCardA();
            String numCard = cardAPI.getCardNumberA();
            double sumOnCard = cardAPI.getMoneyOnCardA();
            Card card = new Card(typeCard, numCard, sumOnCard);
            return card;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getOneCard: " + e);
            return null;
        }
    }

    public boolean loadCards() {
        try {
            for (int i = 0; i < cardsAPIArray.size(); i++) {
                Card card = getOneCard(cardsAPIArray.get(i));
                this.cards.add(card);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "loadCards: " + e);
            return false;
        }
    }

    // --------------------------------------------------

    public Count getOneCount(CountAPI countAPI) {
        try {
            String numCount = countAPI.getNumCountA();
            double moneyOnCount = countAPI.getMoneyOnCountA();
            Count count = new Count(numCount, moneyOnCount);
            return count;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getOneCount: " + e);
            return null;
        }
    }

    public boolean loadCounts() {
        try {
            for (int i = 0; i < countsAPIArray.size(); i++) {
                Count count = getOneCount(countsAPIArray.get(i));
                this.counts.add(count);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "loadCounts: " + e);
            return false;
        }
    }

    // --------------------------------------------------

    public Credit getOneCredit(CreditAPI creditAPI) {
        try {
            String typeCredit = creditAPI.getTypeCreditA();
            String payDate = creditAPI.getPayDateA();
            double sumCredit = creditAPI.getSumCreditA();
            Credit credit = new Credit(typeCredit, payDate, sumCredit);
            return credit;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "getOneCredit: " + e);
            return null;
        }
    }

    public boolean loadCredits() {
        try {
            for (int i = 0; i < creditsAPIArray.size(); i++) {
                Credit credit = getOneCredit(creditsAPIArray.get(i));
                this.credits.add(credit);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "loadCredits: " + e);
            return false;
        }
    }

    // --------------------------------------------------

    public boolean loadVisits(Link link) {
        try {
            String jsonUrl = Api.get(link);
            JSONObject jsonObject = new JSONObject(jsonUrl);
            JSONArray jsonArray = jsonObject.getJSONArray("data");
            for (int i = 0; i < jsonArray.length(); i++) {
                String date = jsonArray.getJSONObject(i).getString("year");
                String time = jsonArray.getJSONObject(i).getString("name");
                visits.add(new Visit(date, time));
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "loadVisits: " + e);
            return false;
        }
    }


    public String getToken() {
        return token;
    }

    public ArrayList<Card> getCards() {
        return cards;
    }

    public ArrayList<Count> getCounts() {
        return counts;
    }

    public ArrayList<Credit> getCredits() {
        return credits;
    }

    public ArrayList<Visit> getVisits() {
        return visits;
    }

    // <----------------------------------------------------------------------->
    // <----------------------------------------------------------------------->
    // <----------------------------------------------------------------------->
    // <----------------------------------------------------------------------->
    // <----------------------------------------------------------------------->

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(middleName);
        dest.writeString(login);
        dest.writeString(token);
        dest.writeList(cards);
        dest.writeList(counts);
        dest.writeList(credits);
        dest.writeList(visits);
    }
}


class  storage{
    static User user;
}