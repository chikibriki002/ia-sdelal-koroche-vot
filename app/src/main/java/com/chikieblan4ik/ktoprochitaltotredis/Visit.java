package com.chikieblan4ik.ktoprochitaltotredis;

import android.os.Parcel;
import android.os.Parcelable;

public class Visit implements Parcelable {

    private String dateVisit;
    private String timeVisit;

    public Visit(String dateVisit, String timeVisit) {
        this.dateVisit = dateVisit;
        this.timeVisit = timeVisit;
    }

    protected Visit(Parcel in) {
        dateVisit = in.readString();
        timeVisit = in.readString();
    }

    public static final Creator<Visit> CREATOR = new Creator<Visit>() {
        @Override
        public Visit createFromParcel(Parcel in) {
            return new Visit(in);
        }

        @Override
        public Visit[] newArray(int size) {
            return new Visit[size];
        }
    };

    public String getDateVisit() {
        return dateVisit;
    }

    public String getTimeVisit() {
        return timeVisit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dateVisit);
        dest.writeString(timeVisit);
    }
}
