package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.AsyncTaskLoader;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BankActivity extends AppCompatActivity {

    public static final String TAG = "bank";
    private ImageView btnProfile;

    private RecyclerView rvCards;
    private RecyclerView rvCounts;
    private RecyclerView rvCredits;

    RecyclerAdapterCard adapterCard;
    RecyclerAdapterCount adapterCount;
    RecyclerAdapterCredit adapterCredit;

//    ArrayList<CardAPI> cardsAPIArray;
//    ArrayList<CountAPI> countsAPIArray;
//    ArrayList<CreditAPI> creditsAPIArray;

    /* static */ User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank);
        getSupportActionBar().hide();
        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_home, R.id.navigation_dashboard, R.id.navigation_notifications)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
        rvCards = findViewById(R.id.rvCards);
        rvCounts = findViewById(R.id.rvCounts);
        rvCredits = findViewById(R.id.rvCredits);
        btnProfile = findViewById(R.id.btnProfile);

//        Bundle arguments = getIntent().getExtras();
//        if (arguments != null) {
//            user = arguments.getParcelable("user");
//            Log.i(TAG, "onCreate: " + user);
//        } else {
//            Log.i(TAG, "Короче то че ты передал его нету ((9");
//        }
        String token = getIntent().getStringExtra("token");
        Log.i(TAG, "onCreateTOKENENENEN: " + token);
        user = new User(token);

//        Runnable runLoadCards = new Runnable() {
//            @Override
//            public void run() {
//                user.loadCards();
//            }
//        };
//
//        Runnable runLoadCounts = new Runnable() {
//            @Override
//            public void run() {
//                user.loadCounts();
//            }
//        };
//
//        Runnable runLoadCredits = new Runnable() {
//            @Override
//            public void run() {
//                user.loadCredits();
//            }
//        };
//
//        Thread thCards = new Thread(runLoadCards);
//        try {
//            thCards.start();
//            thCards.join();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(TAG, "onCreate: " + e);
//        }
//
//        Thread thCounts = new Thread(runLoadCounts);
//        try {
//            thCounts.start();
//            thCounts.join();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(TAG, "onCreate: " + e);
//        }
//
//        Thread thCredits = new Thread(runLoadCredits);
//        try {
//            thCredits.start();
//            thCredits.join();
//        } catch (Exception e) {
//            e.printStackTrace();
//            Log.i(TAG, "onCreate: " + e);
//        }

        new Thread(new Runnable() {
            public void run() {
                try {
                    user.loadCards();
                    rvCards.post(new Runnable() {
                        @Override
                        public void run() {
                            adapterCard = new RecyclerAdapterCard(getApplicationContext(), user.getCards());
                            adapterCard.setOnCardClickListener(new RecyclerAdapterCard.OnCardClickListener() {
                                @Override
                                public void onCardClicked(int position) {
                                    Intent intent = new Intent(BankActivity.this, CardsActivity.class);
                                    intent.putExtra("position", position);
                                    intent.putExtra("user", user);
                                    startActivity(intent);
                                }
                            });
                            rvCards.setAdapter(adapterCard);
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

//        user.loadCards();
        user.loadCounts();
        user.loadCredits();

        rvCards.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));

        rvCounts.setLayoutManager(new LinearLayoutManager(this));
        adapterCount = new RecyclerAdapterCount(this, user.getCounts());
        adapterCount.setOnCountClickListener(new RecyclerAdapterCount.OnCountClickListener() {
            @Override
            public void onCountClicked(Count count) {

            }
        });
        rvCounts.setAdapter(adapterCount);

        rvCredits.setLayoutManager(new LinearLayoutManager(this));
        adapterCredit = new RecyclerAdapterCredit(this, user.getCredits());
        rvCredits.setAdapter(adapterCredit);

        btnProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BankActivity.this, MenuProfileActivity.class);
                startActivity(intent);
            }
        });
    }
}

class CardAPI {

    public String typeCardA;
    public String cardNumberA;
    public double moneyOnCardA;

    public CardAPI(String typeCard, String cardNumber, double moneyOnCard) {
        this.typeCardA = typeCard;
        this.cardNumberA = cardNumber;
        this.moneyOnCardA = moneyOnCard;
    }

    public String getTypeCardA() {
        return typeCardA;
    }

    public String getCardNumberA() {
        return cardNumberA;
    }

    public double getMoneyOnCardA() {
        return moneyOnCardA;
    }
}

class CountAPI {

    private String numCountA;
    private double moneyOnCountA;

    public CountAPI(String numCount, double moneyOnCount) {
        this.numCountA = numCount;
        this.moneyOnCountA = moneyOnCount;
    }

    public String getNumCountA() {
        return numCountA;
    }

    public double getMoneyOnCountA() {
        return moneyOnCountA;
    }
}

class CreditAPI {

    private String payDateA;
    private String typeCreditA;
    private double sumEveryMonthPayA;
    private double sumCreditA;

    public CreditAPI(String typeCredit, String payDate, double sumEveryMonthPay) {
        this.typeCreditA = typeCredit;
        this.payDateA = payDate;
        this.sumEveryMonthPayA = sumEveryMonthPay;
    }

    public String getPayDateA() {
        return payDateA;
    }

    public String getTypeCreditA() {
        return typeCreditA;
    }

    public double getSumEveryMonthPayA() {
        return sumEveryMonthPayA;
    }

    public double getSumCreditA() {
        return sumCreditA;
    }
}

