package com.chikieblan4ik.ktoprochitaltotredis;

import android.os.Parcel;
import android.os.Parcelable;

public class Credit implements Parcelable {

    private String typeCredit;
    private String dateCredit;
    private double sumCredit;

    public Credit(String typeCredit, String dateCredit, double sumCredit) {
        this.typeCredit = typeCredit;
        this.dateCredit = dateCredit;
        this.sumCredit = sumCredit;
    }

    protected Credit(Parcel in) {
        typeCredit = in.readString();
        dateCredit = in.readString();
        sumCredit = in.readDouble();
    }

    public static final Creator<Credit> CREATOR = new Creator<Credit>() {
        @Override
        public Credit createFromParcel(Parcel in) {
            return new Credit(in);
        }

        @Override
        public Credit[] newArray(int size) {
            return new Credit[size];
        }
    };

    public String getTypeCredit() {
        return typeCredit;
    }

    public String getDateCredit() {
        return dateCredit;
    }

    public double getSumCredit() {
        return sumCredit;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeCredit);
        dest.writeString(dateCredit);
        dest.writeDouble(sumCredit);
    }
}
