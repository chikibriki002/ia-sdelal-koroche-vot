package com.chikieblan4ik.ktoprochitaltotredis;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryVisits extends AppCompatActivity {

    private ListView lvVisits;
    private ImageView btnBack;

    private static final String TAG = "history";

    String url = "https://reqres.in/api/unknown";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_visits);
        getSupportActionBar().hide();
        lvVisits = findViewById(R.id.lvVisits);

        ArrayList<Visit> visits = createVisitsArray();
        AdapterVisit adapterVisit = new AdapterVisit(this, visits);
        lvVisits.setAdapter(adapterVisit);

        btnBack = findViewById(R.id.btnBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HistoryVisits.this, MenuProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    public ArrayList<Visit> createVisitsArray() {
        try {
            ArrayList<Visit> visits = new ArrayList<>();
            String jsonUrl = Api.get(new Link(null, url));
            JSONObject jsonObject = new JSONObject(jsonUrl);
            JSONArray jsonArray = jsonObject.getJSONArray("data")   ;
            for (int i = 0; i < jsonArray.length(); i++) {
                String date = jsonArray.getJSONObject(i).getString("year");
                String time = jsonArray.getJSONObject(i).getString("name");
                visits.add(new Visit(date, time));
            }
            return visits;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "onCreate: " + e);
            return null;
        }
    }
}