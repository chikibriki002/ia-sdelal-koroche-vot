package com.chikieblan4ik.ktoprochitaltotredis;

import android.os.Parcel;
import android.os.Parcelable;

public class Card implements Parcelable {

    private String typeCard;
    private String numberCard;
    private double sumOnCard;

    public Card(String typeCard, String numberCard, double sumOnCard) {
        this.typeCard = typeCard;
        this.numberCard = numberCard;
        this.sumOnCard = sumOnCard;
    }

    protected Card(Parcel in) {
        typeCard = in.readString();
        numberCard = in.readString();
        sumOnCard = in.readDouble();
    }

    public static final Creator<Card> CREATOR = new Creator<Card>() {
        @Override
        public Card createFromParcel(Parcel in) {
            return new Card(in);
        }

        @Override
        public Card[] newArray(int size) {
            return new Card[size];
        }
    };

    public String getTypeCard() {
        return typeCard;
    }

    public String getNumberCard() {
        return numberCard;
    }

    public double getSumOnCard() {
        return sumOnCard;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(typeCard);
        dest.writeString(numberCard);
        dest.writeDouble(sumOnCard);
    }
}
