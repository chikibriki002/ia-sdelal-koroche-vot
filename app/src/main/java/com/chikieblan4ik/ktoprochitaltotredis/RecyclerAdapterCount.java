package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterCount  extends RecyclerView.Adapter<RecyclerAdapterCount.ViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Count> counts;

    private ItemClickListener itemClickListener;
    private OnCountClickListener onCountClickListener;

    public RecyclerAdapterCount(Context context, ArrayList<Count> counts) {
        this.inflater = LayoutInflater.from(context);
        this.counts = counts;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_count, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Count oneCount = counts.get(position);
        holder.tvNumCount.setText(oneCount.getNumberCount());
        holder.tvSumCount.setText(oneCount.getSumOnCount() + "");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCountClickListener.onCountClicked(oneCount);
            }
        });
    }

    @Override
    public int getItemCount() {
        return counts.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvNumCount;
        TextView tvSumCount;
        public ViewHolder(View itemView) {
            super(itemView);
            tvSumCount = itemView.findViewById(R.id.tvSumCount);
            tvNumCount = itemView.findViewById(R.id.tvNumCount);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    public interface OnCountClickListener {
        void onCountClicked(Count count);
    }

    public void setOnCountClickListener(OnCountClickListener onCountClickListener) {
        this.onCountClickListener = onCountClickListener;
    }
}
