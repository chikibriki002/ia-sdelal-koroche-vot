package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class AdapterCredit extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Credit> credits;

    AdapterCredit(Context context, ArrayList<Credit> credits) {
        this.ctx = context;
        this.credits = credits;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return credits.size();
    }

    @Override
    public Object getItem(int position) {
        return credits.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_credit, parent, false);
        }

        Credit oneCredit = credits.get(position);
        ((TextView) (view).findViewById(R.id.tvTypeCredit)).setText(oneCredit.getTypeCredit());
        ((TextView) (view).findViewById(R.id.tvDateCredit)).setText(oneCredit.getDateCredit());
        ((TextView) (view).findViewById(R.id.tvSumCredit)).setText(oneCredit.getSumCredit() + "");

        return view;
    }
}
