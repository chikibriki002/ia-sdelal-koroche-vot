package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class AdapterCount extends BaseAdapter {

    Context ctx;
    LayoutInflater inflater;
    ArrayList<Count> counts;

    AdapterCount(Context context, ArrayList<Count> counts) {
        this.ctx = context;
        this.counts  = counts;
        inflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return counts.size();
    }

    @Override
    public Object getItem(int position) {
        return counts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = inflater.inflate(R.layout.item_count, parent, false);
        }

        Count oneCount = counts.get(position);
        ((TextView) (view).findViewById(R.id.tvNumCount)).setText(oneCount.getNumberCount());
        ((TextView) (view).findViewById(R.id.tvSumCount)).setText(oneCount.getSumOnCount() + "");

        return view;
    }
}
