package com.chikieblan4ik.ktoprochitaltotredis;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapterCard extends RecyclerView.Adapter<RecyclerAdapterCard.ViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<Card> cards;

    private OnCardClickListener onCardClickListener;
    private ItemClickListener itemClickListener;

    public RecyclerAdapterCard(Context context, ArrayList<Card> cards) {
        this.inflater = LayoutInflater.from(context);
        this.cards = cards;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= inflater.inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Card oneCard = cards.get(position);
        holder.tvTypeCard.setText(oneCard.getTypeCard());
        holder.tvNumCard.setText(oneCard.getNumberCard());
        holder.tvSumOnCard.setText(oneCard.getSumOnCard() + "");
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onCardClickListener.onCardClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvTypeCard;
        TextView tvSumOnCard;
        TextView tvNumCard;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvTypeCard = itemView.findViewById(R.id.tvTypeCard);
            tvNumCard = itemView.findViewById(R.id.tvNumCard);
            tvSumOnCard = itemView.findViewById(R.id.tvSumOnCard);
        }

        @Override
        public void onClick(View v) {
            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface ItemClickListener {
        void onItemClick(View v, int position);
    }

    public interface OnCardClickListener {
        void onCardClicked(int position);
    }

    public void setOnCardClickListener(OnCardClickListener onCardClickListener) {
        this.onCardClickListener = onCardClickListener;
    }

}