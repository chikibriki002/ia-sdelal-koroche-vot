package com.chikieblan4ik.ktoprochitaltotredis;

import android.util.Log;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

public class Link {

    private static final String TAG = "link";
    String url;
    Map<String, String> keysValues;

    public Link(Map<String, String> keysValues, String url) {
        if (keysValues != null)
            this.keysValues = keysValues;

        this.url = url;
    }



    public HttpURLConnection openConnection() {
        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            return connection;
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "startConnection: " + e);
            return null;
        }
    }

    public String getStrParams() {
        try {
            StringBuilder constructor = new StringBuilder();
            Iterator iterator = keysValues.keySet().iterator();
            while (iterator.hasNext()) {
                String key = iterator.next().toString();
                constructor.append(key);
                constructor.append("=");
                constructor.append(keysValues.get(key));
                constructor.append("&");
            }
            return constructor.substring(0, constructor.length() - 1);
        } catch (Exception e) {
            e.printStackTrace();
            Log.i(TAG, "createLink: " + e);
            return null;
        }
    }

    public String get() {
        return url + getStrParams();
    }



}
